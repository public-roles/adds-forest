adds-forest
=========

A role to deploy a new adds forest to a existing host

Requirements
------------

tbd

Role Variables
--------------

adds_domain:
  create_dns_delegation: no
  database_path: C:\Windows\NTDS
  dns_domain_name: ansible.vagrant
  domain_mode: Win2012R2
  domain_netbios_name: ANSIBLE
  forest_mode: Win2012R2
  safe_mode_password: password123!
  sysvol_path: C:\Windows\SYSVOL

Dependencies
------------

tbd

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: adds-domain, adds_domain: {{adds_domain}}}

License
-------

BSD
