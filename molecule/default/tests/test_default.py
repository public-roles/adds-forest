import os
import re

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_windows_feature(host):
    feature = host.ansible(
        "win_command", "powershell -c get-windowsfeature", check=False)
    assert re.search(r'AD\-Domain\-Services\s+Installed', feature['stdout'])
    assert re.search(r'RSAT\-AD\-PowerShell\s+Installed', feature['stdout'])


def test_ad_domain(host):
    domain = host.ansible(
        "win_command", "powershell -c get-addomain", check=False)
    assert re.search(r'ansible.vagrant', domain['stdout'])
